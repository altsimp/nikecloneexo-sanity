import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'ruo5dylw',
    dataset: 'production'
  }
})
