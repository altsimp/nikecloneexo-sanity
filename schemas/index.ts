import brand from "./brand";
import category from "./category";
import color from "./color";
import page from "./page";
import product from "./product";
import size from "./size";
import variant from "./variant";

export const schemaTypes = [product, variant, page, brand, category, color, size]
