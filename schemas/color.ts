// schemas/color.js
export default {
  name: 'color',
  type: 'document',
  title: 'Color',
  fields: [
    {
      name: 'value',
      type: 'string',
      title: 'Value',
    },
  ],
  preview: {
    select: {
      title: 'value',
    },
  },
}